package com.example.widisetyawan.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("PubnubService", "PubNub BootReceiver Starting")
        val intent1 = Intent(context, PubnubService::class.java)
        context?.startService(intent1)
        Log.i("PubnubService", "PubNub BootReceiver Started")
    }

}