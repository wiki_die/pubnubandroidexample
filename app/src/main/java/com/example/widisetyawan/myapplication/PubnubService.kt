package com.example.widisetyawan.myapplication

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.os.PowerManager
import com.pubnub.api.Pubnub
import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.Log
import android.widget.Toast
import com.pubnub.api.Callback
import com.pubnub.api.PubnubError
import com.pubnub.api.PubnubException

class PubnubService : Service() {

    var channel = "user-001"
    var pubnub = Pubnub("demo", "sub-c-90749826-e30f-11e8-89eb-46b5aa81648a")
    var wl: PowerManager.WakeLock? = null

    private val handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            val pnMsg = msg.obj.toString()

            val toast = Toast.makeText(applicationContext, pnMsg, Toast.LENGTH_SHORT)
            toast.show()

            val handler = Handler()
            handler.postDelayed(Runnable { toast.cancel() }, 5000)

        }
    }

    private fun notifyUser(message: Any) {

        val msg = handler.obtainMessage()

        try {
            val obj = message as String
            msg.obj = obj
            handler.sendMessage(msg)
            Log.i("Received msg : ", obj)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @SuppressLint("InvalidWakeLockTag")
    override fun onCreate() {
        super.onCreate()
        Toast.makeText(this, "PubnubService created...", Toast.LENGTH_LONG).show()
        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SubscribeAtBoot")
        if (wl != null) {
            wl?.acquire(10*60*1000L /*10 minutes*/)
            Log.i("PUBNUB", "Partial Wake Lock : " + wl?.isHeld())
            Toast.makeText(this, "Partial Wake Lock : " + wl?.isHeld(), Toast.LENGTH_LONG).show()
        }

        Log.i("PUBNUB", "PubnubService created...")
        try {
            pubnub.subscribe(channel, object : Callback() {
                fun connectCallback(channel: String) {
                    notifyUser("CONNECT on channel:$channel")
                }

                fun disconnectCallback(channel: String) {
                    notifyUser("DISCONNECT on channel:$channel")
                }

                fun reconnectCallback(channel: String) {
                    notifyUser("RECONNECT on channel:$channel")
                }

                override fun successCallback(channel: String, message: Any) {
                    notifyUser(channel + " " + message.toString())
                }

                override fun errorCallback(channel: String, message: PubnubError) {
                    notifyUser(channel + " " + message.toString())
                }
            })
        } catch (e: PubnubException) {

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (wl != null) {
            wl?.release()
            Log.i("PUBNUB", "Partial Wake Lock : " + wl?.isHeld())
            Toast.makeText(this, "Partial Wake Lock : " + wl?.isHeld(), Toast.LENGTH_LONG).show()
            wl = null
        }
        Toast.makeText(this, "PubnubService destroyed...", Toast.LENGTH_LONG).show()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

}